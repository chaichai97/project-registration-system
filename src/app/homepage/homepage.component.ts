import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  ngOnInit() {
  }
  constructor(private router:Router){}
  pushPageStudent(){
    this.router.navigate(['Register']);
    }
    pushPageTeacher(){
      this.router.navigate(['teacher']);
      }
  pushPageAdmin(){
    this.router.navigate(['login']);
    }
}
