import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AdminService, addYear, YearsData } from 'src/app/service/admin.service';
import { Observable } from 'rxjs/Observable';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { PapaParseService } from 'ngx-papaparse';
import 'rxjs/add/operator/map';

export interface StudentData {
  student_id: string;
  name: string;
  gpa: number;
}


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  displayedColumns: string[] = ['student_id','name', 'gpa'];
  displayedColumns2: string[] = ['year', 'status', 'edit'];
  displayedColumns3: string[] = ['year','edit'];
  dataSource: MatTableDataSource<StudentData>;
  yearSource: MatTableDataSource<any>;

  mobileQuery: MediaQueryList;
  getAcademic_years: Observable<any>;
  getData: Observable<any>;
  getData2: Observable<any>;
  getSelectYear: Observable<any>;
  getStudent_Books: any[];
  getTeacher_Reg: any[];
  studentData: any[];
  statusSort: any[];

  seatUpdate: AngularFireList<any>;

  years: Observable<any>;
  selectFormControl: FormControl;
  selectFormControl2: FormControl;
  yearData: any[];
  yearsDataList: any[];

  addYear: addYear;
  form: FormGroup;
  addNewYear: FormGroup;
  student_status: string;
  studentBooks_status: string;
  studentLeft_status: string;

  teacher_status: string;
  teacherReg_status: string;
  teacherSeatCount_status: string;
  teacherSeat_status: string;

  studentCount = 0;
  teacherSeat = 0;
  teacherSeatCount = 0;

  showDetail = false;
  upload_status = false;
  showUpload = false;
  showManage = false;
  showManageDB = true;
  checkDuplicate = false;
  showDuplicate = true;
  errTitle = true;
  showbutton;

  countDuplicate = 0;

  private _mobileQueryListener: () => void;

  constructor(private router: Router, changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher, private adminService: AdminService, private db: AngularFireDatabase,
    private papa: PapaParseService, private formBuilder: FormBuilder, ) {

    this.dataSource = new MatTableDataSource();

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.errTitle = true;
    this.teacherSeatCount = 0;
    this.teacherSeat = 0;
    this.countDuplicate = 0;
    this.showUpload = false;
    this.showDetail = false;
    this.showManage = false;
    this.showManageDB = true;
    this.checkDuplicate = false;
    this.showDuplicate = true;
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.getAcademic_years = this.adminService.getAcdemic_years();
    this.addYear = new addYear();
    this.addNewYear = this.formBuilder.group({
      year: ['', Validators.required]
    });
    this.getAcademic_years.forEach(data => {
      this.years = data;
    });


    this.adminService.getAcademicYears().subscribe(
      list => {
        this.yearsDataList = [];
        list.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;
          this.yearsDataList.push(a as YearsData);
        })
      });

    //console.log(this.yearsDataList);
    //this.yearSource = new MatTableDataSource(Object.values(this.years));
    this.form = this.formBuilder.group({
      csv: ['']
    });

    this.selectFormControl = new FormControl(null, [Validators.required]);
    this.selectFormControl2 = new FormControl();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  signOut() {
    this.router.navigate(['login']);
  }
  shouldRun = true;
  show() {
    //console.log(this.selectFormControl.value);
    if (this.selectFormControl.value == null) {
      this.showDetail = false;
    }
    else {
      this.showDetail = true; this.selectFormControl.value
      this.getSelectYear = this.adminService.getSelectYear(this.selectFormControl.value);
      this.getSelectYear.forEach(data => {
        this.yearData = data;
      });
      this.getData = this.adminService.getStudentData(this.selectFormControl.value);
      this.getData.forEach(data => {
        this.student_status = "นักศึกษา : " + data.length + " คน"
      });

      this.getData = this.adminService.getTeacherData();
      this.getData.forEach(data => {
        this.teacher_status = "อาจารย์ : " + data.length + " คน"
      });

      this.getData = this.adminService.getTeacher_Reg(this.selectFormControl.value);
      this.getData.forEach(data => {
        this.teacherReg_status = "อาจารย์ลงทะเบียน : " + data.length + " คน"
      });

      this.getData = this.adminService.getStudent_Books(this.selectFormControl.value);
      this.getData.forEach(data => {
        this.studentBooks_status = "นักศึกษาลงทะเบียน : " + data.length + " คน"
      });

      this.getData = this.adminService.getTeacher_Reg(this.selectFormControl.value);
      this.getData.forEach(data => {
        for (let i = 0; i < data.length; i++) {
          this.teacherSeatCount = this.teacherSeatCount + data[i].count;
        }
        this.teacherSeatCount_status = "ที่ว่างในการลงทะเบียน : " + this.teacherSeatCount + " ที่"
        this.teacherSeatCount = 0;
      });

      this.getData = this.adminService.getTeacher_Reg(this.selectFormControl.value);
      this.getData.forEach(data => {
        for (let i = 0; i < data.length; i++) {
          this.teacherSeat = this.teacherSeat + data[i].seat;
        }
        this.teacherSeat_status = "จำนวนที่รับนักศึกษา : " + this.teacherSeat + " คน"
        this.teacherSeat = 0;
      });

      this.getData = this.adminService.getStudentData(this.selectFormControl.value);
      this.getData2 = this.adminService.getStudent_Books(this.selectFormControl.value);
      this.getData.forEach(data1 => {
        this.getData2.forEach(data2 => {
          this.studentLeft_status = "นักศึกษายังไม่ได้ลงทะเบียน : " + (data1.length - data2.length) + " คน"
        });
      });
      // this.getStudent_Books = this.adminService.getStudent_Books(this.selectFormControl.value);
      // this.getTeacher_Reg = this.adminService.getTeacher_Reg(this.selectFormControl.value);

      this.adminService.getStudent_Books(this.selectFormControl.value).forEach(data => {
        this.getStudent_Books = data;
      });

      this.adminService.getTeacher_Reg(this.selectFormControl.value).forEach(data => {
        this.getTeacher_Reg = data;
      });

      this.adminService.getStudentData(this.selectFormControl.value).forEach(data => {
        this.studentData = data;
      });
      //console.log(this.getStudent_Books);
      // for (let i = 3; i < this.getTeacher_Reg.length + 3; i++) {
      //   console.log(Object.values(this.getStudent_Books[0])[i]);
      // }
      this.adminService.getAcdemic_years().forEach(data => {
        for (let i = 0; i < data.length; i++) {
          this.statusSort = data;
          if (data[i].key == this.selectFormControl.value) {
            this.showbutton = this.statusSort[i].statusSort;
            break;
          }
        }
      });
      //console.log(this.showbutton);
    }

  }
  show2() {

    for (let j = 0; j < this.getStudent_Books.length; j++) {
      for (let i = 0; i < this.getStudent_Books.length - 1; i++) {
        if (this.getStudent_Books[i].gpa > this.getStudent_Books[i + 1].gpa) {
          this.getStudent_Books[i] = this.getStudent_Books[i];
        } else if (this.getStudent_Books[i].gpa < this.getStudent_Books[i + 1].gpa) {
          let swap = this.getStudent_Books[i + 1];
          this.getStudent_Books[i + 1] = this.getStudent_Books[i];
          this.getStudent_Books[i] = swap;
        } else {
          for (let k = 0; k < 6; k++) {
            if (Number(this.getStudent_Books[i].Time.split(/[\s-:]+/)[k]) > Number(this.getStudent_Books[i + 1].Time.split(/[\s-:]+/)[k])) {
              let swap = this.getStudent_Books[i + 1];
              this.getStudent_Books[i + 1] = this.getStudent_Books[i];
              this.getStudent_Books[i] = swap;
              break;
            } else if (Number(this.getStudent_Books[i].Time.split(/[\s-:]+/)[k]) < Number(this.getStudent_Books[i + 1].Time.split(/[\s-:]+/)[k])) {
              this.getStudent_Books[i] = this.getStudent_Books[i];
              break;
            }
          }
        }
      }
    }
    //console.log("Sort");
    //console.log(this.getStudent_Books);

    for (let i = 0; i < this.getStudent_Books.length; i++) {
      let next = 3;
      for (next; next < this.getTeacher_Reg.length + 3; next++) {
        for (let j = 0; j < this.getTeacher_Reg.length; j++) {
          //console.log(next + " : " + j);
          if (Object.values(this.getStudent_Books[i])[next] == this.getTeacher_Reg[j].fullName && this.getTeacher_Reg[j].count == 0) {
            //console.log(this.getTeacher_Reg[j].fullName + " seat 0")
            break;
          }
          if (Object.values(this.getStudent_Books[i])[next] == this.getTeacher_Reg[j].fullName) {
            //console.log(Object.values(this.getStudent_Books[i])[next] + "==" + this.getTeacher_Reg[j].fullName);
            //console.log("seat Before : " + this.getTeacher_Reg[j].seat)

            var teacher = this.db.list('academic_years/' + this.selectFormControl.value + '/books/' + this.getStudent_Books[i].key);
            teacher.set(`teacher`, {
              teacher_id: this.getTeacher_Reg[j].key,
              fullName: this.getTeacher_Reg[j].fullName

            });

            var seatRef = this.db.list('academic_years/' + this.selectFormControl.value + '/teacher_reg/' + this.getTeacher_Reg[j].key + '/seatNumber');
            seatRef.set(`${this.getStudent_Books[i].key}`, {
              std_id: this.getStudent_Books[i].key,
              name: this.getStudent_Books[i].StudentName,
              gpa: this.getStudent_Books[i].gpa
            });


            let seat = this.getTeacher_Reg[j].count;
            seat--;
            this.getTeacher_Reg[j].count = seat;
            this.seatUpdate = this.db.list('academic_years/' + this.selectFormControl.value + '/teacher_reg/');
            this.seatUpdate.update(this.getTeacher_Reg[j].key, {
              count: seat
            });


            //console.log("seat After : " + this.getTeacher_Reg[j].seat);
            //console.log("student : " + this.getStudent_Books[i].StudentName);
            //console.log("get teacher :" + this.getTeacher_Reg[j].fullName);

            next = 9999;
            break;
          }

        }

      }

    }
    for (let i = 0; i < this.getStudent_Books.length; i++) {
      this.studentData = this.studentData.filter(person => person.student_id != this.getStudent_Books[i].key)
    }
    //console.log("Student_Data not reg Sort");
    //console.log(this.studentData);
    //console.log(Number(this.getTeacher_Reg[0].count));

    for (let i = 0; i < this.studentData.length; i++) {
      for (let j = 0; j < this.getTeacher_Reg.length; j++) {

        if (Number(this.getTeacher_Reg[j].count) > 0) {

          var seatRef = this.db.list('academic_years/' + this.selectFormControl.value + '/teacher_reg/' + this.getTeacher_Reg[j].key + '/seatNumber');
          seatRef.set(`${this.studentData[i].student_id}`, {
            std_id: this.studentData[i].student_id,
            name: this.studentData[i].name,
            gpa: this.studentData[i].gpa
          });

          var seatRef = this.db.list('academic_years/' + this.selectFormControl.value + '/student_unsigned');
          seatRef.set(`${this.studentData[i].student_id}`, {
            std_id: this.studentData[i].student_id,
            name: this.studentData[i].name,
            gpa: this.studentData[i].gpa,
            teacher: {
              teacher_id: this.getTeacher_Reg[j].key,
              fullName: this.getTeacher_Reg[j].fullName
            }
          });



          let seat = this.getTeacher_Reg[j].count;
          seat--;
          this.getTeacher_Reg[j].count = seat;
          this.seatUpdate = this.db.list('academic_years/' + this.selectFormControl.value + '/teacher_reg/');
          this.seatUpdate.update(this.getTeacher_Reg[j].key, {
            count: seat
          });
          //console.log("seat After : " + this.getTeacher_Reg[j].seat);
          //console.log("student : " + this.studentData[i].name);
          //console.log("get teacher :" + this.getTeacher_Reg[j].fullName);
          break;
        }
      }
    }
    this.showbutton = false;
    this.seatUpdate = this.db.list('academic_years');
    this.seatUpdate.update(this.selectFormControl.value, {
      statusSort: false
    });


  }
  resetData() {
    this.seatUpdate = this.db.list('academic_years');
    this.seatUpdate.update(this.selectFormControl.value, {
      statusSort: true
    });



    for (let j = 0; j < this.getTeacher_Reg.length; j++) {
      let seat = this.getTeacher_Reg[j].seat;
      var seatUpdate = this.db.list('academic_years/' + this.selectFormControl.value + '/teacher_reg/');
      seatUpdate.update(this.getTeacher_Reg[j].key, {
        count: seat
      });

      var remove = this.db.list('academic_years/' + this.selectFormControl.value + '/teacher_reg/' + this.getTeacher_Reg[j].key + '/seatNumber');
      remove.remove();
    }

    var remove = this.db.list('academic_years/' + this.selectFormControl.value + '/student_unsigned');
    remove.remove();

    for (let j = 0; j < this.getStudent_Books.length; j++) {
      var remove = this.db.list('academic_years/' + this.selectFormControl.value + '/books/' + this.getStudent_Books[j].key + '/teacher');
      remove.remove();
    }
  }

  test = [];
  // student upload
  student_data = [];
  student_data_count = 0;
  check_data_student(evt) {
    if (evt.target.files.length > 0) {
      this.student_data_count = evt.target.files.length;
      this.student_data = [];
      var files = evt.target.files; // FileList object
      var file = files[0];
      var reader = new FileReader();
      reader.readAsText(file);
      reader.onload = (event: any) => {
        var csv = event.target.result; // Content of CSV file
        this.papa.parse(csv, {
          skipEmptyLines: true,
          header: true,
          complete: (results) => {
            //console.log(results.meta.fields);
            if (results.meta.fields[0] == 'student_id' &&
              results.meta.fields[1] == 'name' &&
              results.meta.fields[2] == 'gpa'
            ) {
              this.errTitle = true;
            } else {
              this.errTitle = false;
              //console.log('incorract');
            }
            for (let i = 0; i < results.data.length; i++) {
              let orderDetails = {
                order_id: results.data[i].Address,
                age: results.data[i].Age
              };
              //this.test.push(orderDetails);
              //console.log('Round' + i, results.data[i]);
              this.student_data[i] = results.data[i]
              //this.db.list("/std_data").push(results.data[i]);
            }
          }
        });
      }
      //console.log("Data :", this.student_data);
    } else {
      this.student_data_count = evt.target.files.length;
    }
  }
  next_upload() {
    //console.log(this.student_data_count);
    const student = Object.values(this.student_data);
    this.dataSource = new MatTableDataSource(student);
    if (this.student_data_count > 0) {
      //console.log(this.student_data.length);
      // for (let i = 0; i < this.student_data.length; i++) {
      //   this.db.list('academic_years/' + this.selectFormControl.value + "/student_data").push(this.student_data[i]);

      // }
      this.upload_status = true;
    } else {
      this.upload_status = false;
    }
  }
  upload() {
    //console.log(this.student_data.length);
    for (let i = 0; i < this.student_data.length; i++) {
      this.db.list('academic_years/' + this.selectFormControl.value + "/student_data").push(this.student_data[i]);
    }
  }
  showUploadCard() {
    this.showUpload = true;
  }
  resetStep() {
    this.showUpload = false;
  }
  openManage() {
    this.showManage = true;
  }
  closeManage() {
    this.showManage = false;
  }

  openManageDatabase() {
    //console.log(this.yearsDataList);
    //console.log(this.yearData);
    this.showManageDB = false;
  }
  closeManageDatabase() {
    this.showManageDB = true;
  }
  cheackYear() {
    this.checkDuplicate = false;
    //console.log(this.addYear.year);
    this.getAcademic_years.forEach(data => {
      for (let i = 0; i < data.length; i++) {
        //console.log(data[i].key);
        if (this.addYear.year == '' || this.addYear.year == undefined) {
          this.showDuplicate = true;
          this.addNewYear.reset;
          break;
        }
        if (this.addYear.year == data[i].key) {
          this.checkDuplicate = true;
          this.showDuplicate = true;
          this.addNewYear.reset;
          break;
        } else {
          this.showDuplicate = false;
          this.addNewYear.reset;
        }

      }
    });
  }
  insertYear() {
    //console.log(this.addYear.year);
    var seatRef = this.db.list(`academic_years/`);
    seatRef.set(`${this.addYear.year}`, {
      status: true,
      statusSort: false
    });
  }
  deleteDatabase(key: string){
    var seatRef = this.db.list(`academic_years/`);
    seatRef.remove(`${key}`);
  }
  deleteTable(key: string){
    var seatRef = this.db.list(`academic_years/`+this.selectFormControl.value);
    seatRef.remove(`${key}`);

  }
  update(key: string, status: string) {
    //console.log(key);
    //console.log(status);
    var seatRef = this.db.list(`academic_years/`);
    seatRef.update(`${key}`, {
      status: status
    });
    this.selectFormControl2.reset;
  }

}
