import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { PapaParseService } from 'ngx-papaparse';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { AdminService, Admin} from 'src/app/service/admin.service';
import {Router} from '@angular/router';
import { observable } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  check_data = false;
  show_alert = false;

  up_student = false;
  up_teacher = false;

  upload_status = false;
  student_status: string;
  teacher_status: string;


  SignIn: FormGroup;
  admin: Admin;
  getStudentData: Observable<any>;
  getTeacherData: Observable<any>;
  getAdminData: Observable<any>;
  getStudent_Books: Observable<any>;
  getTeacher_Reg: Observable<any>;
  Student_Books: Observable<any>;
  Teacher_reg: Observable<any>;
  seatUpdate: AngularFireList<any>;
  teacherSeat = 0;

  check_TchLogin = false;
  getTchLogin : Observable<any>;
  show_alert2 = false;
  tchKey:string;
  adminKey:string;

  constructor(public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private adminService: AdminService,
    private db: AngularFireDatabase,
    private papa: PapaParseService,
    private router:Router) { }
  form: FormGroup;

  ngOnInit() {
    this.check_data = false;
    this.getAdminData = this.adminService.getAdminData();
    this.admin = new Admin();

    this.check_TchLogin = false;
    this.getTchLogin = this.adminService.getTchLogin();

    this.SignIn = this.formBuilder.group({
      user: [null, Validators.required],
      password: [null, Validators.required]
    });
    this.form = this.formBuilder.group({
      csv: ['']
    });

  }

  show() {
    this.getStudent_Books.forEach(Student_data => {
      this.getTeacher_Reg.forEach(Teacher_data => {
        for (let i = 0; i < Student_data.length; i++) {
          console.log(Object.values(Student_data[i])[1]);
          let next = 2;
          for (next; next < 12; next++) {

            for (let j = 0; j < Teacher_data.length; j++) {
              console.log(next + " : " + j);
              if (Object.values(Student_data[i])[next] == Teacher_data[j].fullName && Teacher_data[j].seat == 0) {
                console.log(Teacher_data[j].fullName + " seat 0")
                break;
              }

              if (Object.values(Student_data[i])[next] == Teacher_data[j].fullName) {
                console.log(Object.values(Student_data[i])[next] + "==" + Teacher_data[j].fullName);
                console.log("seat Before : " + Teacher_data[j].seat)
                // var student_project = this.db.list(`student_project/` + Teacher_data[j].key);
                // student_project.set(`${Student_data[i].key}`, {
                //   std_id: Student_data[i].key
                // });

                var seatRef = this.db.list(`teacher_reg/`+Teacher_data[j].key+'/seatNumber');
                seatRef.set(`${Student_data[i].key}`,{
                  std_id: Student_data[i].key     
                })

                let seat = Teacher_data[j].seat;
                seat--;
                Teacher_data[j].seat = seat;
                this.seatUpdate = this.db.list(`teacher_reg/`);
                this.seatUpdate.update(Teacher_data[j].key, {
                  count: seat
                });
                console.log("seat After : " + Teacher_data[j].seat)
                next = 12;
                break;
              }

            }

            console.log("------------------------------");
          }
          console.log("------------------------------");
        }
      });
    });

  }

  nextpage(){
    this.router.navigate(['dashboard',this.adminKey]);
  }
  nextpage2(){
    this.router.navigate(['teacher',this.tchKey]);
  }

  check_studentId() {
    this.getAdminData.forEach(data => {
      for (let i = 0; i < data.length; i++) {
        console.log(data[i]);
        if (this.admin.user == data[i].user && this.admin.password == data[i].password) {
          this.check_data = true;
          this.show_alert = false;
          this.admin.user = data[i].user;
          this.adminKey = data[i].key
          console.log("Data : " + data[i].user + " " + data[i].password);
          this.SignIn.reset;
          break;

        } else {
          this.show_alert = true;
          this.check_data = false;
          this.SignIn.reset;
          console.log("No data");
        }
      }
    });

  }
 
  check_tchlogin() {
    this.getTchLogin.forEach(data => {
      for (let i = 0; i < data.length; i++) {
        console.log(data[i]);
        if (this.admin.user == data[i].user && this.admin.password == data[i].password) {
          this.check_TchLogin = true;
          this.show_alert2 = false;
          this.admin.user = data[i].user;
          this.tchKey = data[i].key;
          console.log("Data : " + data[i].user + " " + data[i].password);
          this.SignIn.reset;
          break;

        } else {
          this.show_alert2 = true;
          this.check_TchLogin = false;
          this.SignIn.reset;
          console.log("No data",this.admin.user);
        }
      }
    });

  }

  upload_student() {
    this.up_student = true;
  }
  upload_teacher() {
    this.up_teacher = true;
  }
  back() {
    this.up_student = false;
    this.up_teacher = false;
    this.show_alert = true;
    this.check_data = false;
    this.admin.password = '';
    this.upload_status = false;
  }







  test = [];
  // student upload
  student_data = [];
  student_data_count = 0;
  check_data_student(evt) {
    if (evt.target.files.length > 0) {
      this.student_data_count = evt.target.files.length;
      this.student_data = [];
      var files = evt.target.files; // FileList object
      var file = files[0];
      var reader = new FileReader();
      reader.readAsText(file);
      reader.onload = (event: any) => {
        var csv = event.target.result; // Content of CSV file
        this.papa.parse(csv, {
          skipEmptyLines: true,
          header: true,
          complete: (results) => {
            for (let i = 0; i < results.data.length; i++) {
              let orderDetails = {
                order_id: results.data[i].Address,
                age: results.data[i].Age
              };
              //this.test.push(orderDetails);
              console.log('Round' + i, results.data[i]);
              this.student_data[i] = results.data[i]
              //this.db.list("/std_data").push(results.data[i]);
            }
          }
        });
      }
      console.log("Data :", this.student_data);
    } else {
      this.student_data_count = evt.target.files.length;
    }
  }
  upload_student_data() {
    console.log(this.student_data_count);
    if (this.student_data_count > 0) {
      console.log(this.student_data.length);
      for (let i = 0; i < this.student_data.length; i++) {
        this.db.list("/student_data").push(this.student_data[i]);

      }
      this.upload_status = true;
    } else {
      this.dialog.open(DialogAlert);
    }
  }
  // teacher upload
  teacher_data = [];
  teacher_data_count = 0;
  check_data_teacher(evt) {
    if (evt.target.files.length > 0) {
      this.teacher_data_count = evt.target.files.length;
      this.teacher_data = [];
      var files = evt.target.files; // FileList object
      var file = files[0];
      var reader = new FileReader();
      reader.readAsText(file);
      reader.onload = (event: any) => {
        var csv = event.target.result; // Content of CSV file
        this.papa.parse(csv, {
          skipEmptyLines: true,
          header: true,
          complete: (results) => {
            for (let i = 0; i < results.data.length; i++) {
              let orderDetails = {
                order_id: results.data[i].Address,
                age: results.data[i].Age
              };
              //this.test.push(orderDetails);
              console.log('Round' + i, results.data[i]);
              this.teacher_data[i] = results.data[i]
              //this.db.list("/std_data").push(results.data[i]);
            }
          }
        });
      }
      console.log("Data :", this.teacher_data);
    } else {
      this.teacher_data_count = evt.target.files.length;
    }
  }
  upload_teacher_data() {
    console.log(this.teacher_data_count);
    if (this.teacher_data_count > 0) {
      console.log(this.teacher_data.length);
      for (let i = 0; i < this.teacher_data.length; i++) {
        this.db.list("/teacher_data").push(this.teacher_data[i]);

      }
      this.upload_status = true;
    } else {
      this.dialog.open(DialogAlert);
    }
  }
  homepage(){
    this.router.navigate(['']);
  }
}

@Component({
  selector: 'dialog-alert',
  templateUrl: './admin.dialogAlert.html',
})
export class DialogAlert { }
