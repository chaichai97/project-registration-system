
export class Books { 
  name:string;
  id: string = '';
  gpa:string;
  selectedOption01: string = null;
  selectedOption02: string = null;
  selectedOption03: string = null;
  selectedOption04: string = null;
  selectedOption05: string = null;
  selectedOption06: string = null;
  selectedOption07: string = null;
  selectedOption08: string = null;
  selectedOption09: string = null;
  selectedOption10: string = null;
  selectedOption11: string = null;
  selectedOption12: string = null;
  selectedOption13: string = null;
  selectedOption14: string = null;
  selectedOption15: string = null;
  selectedOption16: string = null;
  selectedOption17: string = null;
  selectedOption18: string = null;
  selectedOption19: string = null;
  selectedOption20: string = null;
  time:string=new Date().toISOString().split('T')[0] +" "+new Date().toTimeString().split('GMT')[0].trim();
  status: boolean;
}
