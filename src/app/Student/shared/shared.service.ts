import { Injectable, ɵtextBinding, Output, EventEmitter } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAction } from "angularfire2/database";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Books } from "./books";
import "rxjs/add/operator/switchMap";
import { map } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class SharedService {
  items$: Observable<AngularFireAction<firebase.database.DataSnapshot>[]>;
  size$: BehaviorSubject<string | null>;
  bookData: any;
  bookTeacherData: any;
  chkUnsign = false;
  chkBookTeacher = false;
  @Output() chkStatusDeadline = new EventEmitter();
  constructor(private db: AngularFireDatabase) {}

  getStudent(select: string) {
    return this.db
      .list("academic_years/" + select + "/student_data")
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(
            item => ({ key: item.payload.key, ...item.payload.val() }),
            console.log()
          );
        })
      );
  }
  getTeacher(select: string) {
    return this.db
      .list("academic_years/" + select + "/teacher_reg")
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(item => ({
            key: item.payload.key,
            ...item.payload.val()
          }));
        })
      );
  }
  getYears() {
    return this.db
      .list("academic_years")
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(
            item => ({ key: item.payload.key, ...item.payload.val() }),
            console.log()
          );
        })
      );
  }
  addData(books: Books, select: string) {
    var booksRef = this.db.list("academic_years/" + select + "/books");
    booksRef.set(`${books.id}`, {
      Teacher01: books.selectedOption01,
      Teacher02: books.selectedOption02,
      Teacher03: books.selectedOption03,
      Teacher04: books.selectedOption04,
      Teacher05: books.selectedOption05,
      Teacher06: books.selectedOption06,
      Teacher07: books.selectedOption07,
      Teacher08: books.selectedOption08,
      Teacher09: books.selectedOption09,
      Teacher10: books.selectedOption10,
      Teacher11: books.selectedOption11,
      Teacher12: books.selectedOption12,
      Teacher13: books.selectedOption13,
      Teacher14: books.selectedOption14,
      Teacher15: books.selectedOption15,
      Teacher16: books.selectedOption16,
      Teacher17: books.selectedOption17,
      Teacher18: books.selectedOption18,
      Teacher19: books.selectedOption19,
      Teacher20: books.selectedOption20,
      StudentID: books.id,
      StudentName: books.name,
      Time: books.time,
      gpa: books.gpa
    });
  }
  getBooks(select: string) {
    return this.db
      .list("academic_years/" + select + "/books")
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(item => ({
            key: item.payload.key,
            ...item.payload.val()
          }));
        })
      );
  }
  getStudent_Unsign(select: string) {
    return this.db
      .list("academic_years/" + select + "/student_unsigned")
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(item => ({
            key: item.payload.key,
            ...item.payload.val()
          }));
        })
      );
  }
  getDate(select: string) {
    return this.db
      .list("academic_years/" + select + "/date_reg")
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(item => ({
            key: item.payload.key,
            ...item.payload.val()
          }));
        })
      );
  }
  addStatus(select: string) {
    console.log("addStatus");
    var booksRef = this.db.list("academic_years/" + select);
    booksRef.set("statusTime", {
      statusDeadline: true
    });
  }
  getStatusDeadline(select: string) {
    return this.db
      .list("academic_years/" + select + "/statusTime")
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(item => ({
            key: item.payload.key,
            val: item.payload.val()
          }));
        })
      );
  }
}
