import { Component, OnInit, Inject, EventEmitter,OnDestroy } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { Books } from "../shared/books";
import "rxjs/add/operator/switchMap";
import { SharedService } from "../shared/shared.service";
import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireAction
} from "angularfire2/database";
import { NzMessageService } from "ng-zorro-antd";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import {
  MatDialog,
  MatTableDataSource,
  MatSort,
  MatDialogConfig,
  MAT_DIALOG_DATA
} from "@angular/material";
import { NzNotificationService } from "ng-zorro-antd";
@Component({
  selector: "app-register-student",
  templateUrl: "./register-student.component.html",
  styleUrls: ["./register-student.component.css"]
})
export class RegisterStudentComponent implements OnInit, OnDestroy {
  btnClick = new EventEmitter();
  books: Books;
  key: string;
  getAll: Observable<any>;
  getBook: Observable<any>;
  gettea: Observable<any>;
  getyear: Observable<any>;
  getdate: Observable<any>;
  getTeacherData: any;
  teacher_key: any[];
  teacher_Datakey: any[];
  teacher_data: any[];
  teacherData: any[];
  isLinear = true;
  firstFormGroup: FormGroup;
  firstGroup: FormGroup;
  selectFormControl: FormControl;
  secondFormGroup: FormGroup;
  isEditable = false;
  check_id = false;
  book: any;
  gpa: any;
  years: Observable<any>;
  dateTime: any;
  time;
  showDeadline = false;
  isDeadLine = false;
  teacher_name;
  shows = true;
  teaLen: number;
  getStatusDeadline;
  chk_option_val = {
    chkOption01: false,
    chkOption02: false,
    chkOption03: false,
    chkOption04: false,
    chkOption05: false,
    chkOption06: false,
    chkOption07: false,
    chkOption08: false,
    chkOption09: false,
    chkOption10: false,
    chkOption11: false,
    chkOption12: false,
    chkOption13: false,
    chkOption14: false,
    chkOption15: false,
    chkOption16: false,
    chkOption17: false,
    chkOption18: false,
    chkOption19: false,
    chkOption20: false
  };
  cancel(stepper): void {
    this.nzMessageService.error("ไม่บันทึกข้อมูล");
    this.check_book();
    stepper.reset();
    this.notification.error(
      "ไม่บันทึกข้อมูล",
      `กรุณาบันทึกข้อมูลการเลือกอันดับอาจารย์ด้วย`
    );
  }

  confirm(): void {
    this.nzMessageService.warning("ตรวจสอบข้อมูลและกดบันทึก");
  }
  constructor(
    private fg: FormBuilder,
    private sharedService: SharedService,
    private db: AngularFireDatabase,
    private nzMessageService: NzMessageService,
    private datePipe: DatePipe,
    private router: Router,
    public dialog: MatDialog,
    private notification: NzNotificationService
  ) {}

  ngOnInit() {
    this.time = this.datePipe.transform(new Date(), "yyyy-MM-dd h:mm:ss a");
    this.check_id = false;
    this.getyear = this.sharedService.getYears();
    this.getyear.forEach(data => {
      this.years = data;
    });

    this.books = new Books();
    this.firstFormGroup = this.fg.group({
      firstCtrl: [
        "",
        [Validators.required, Validators.minLength(8), Validators.maxLength(8)]
      ]
    });
    this.firstGroup = this.fg.group({
      firstCtrl1: [null, [Validators.required]]
    });
    this.selectFormControl = new FormControl(null, [Validators.required]);
    this.secondValidate(this.teaLen);
  }
  ngOnDestroy() {
    this.getStatusDeadline = null;
  }

  openDialogTable() {
    this.shows = true;
    this.dialog.open(RegisterStudentDialog, {
      data: {
        teacher_name: this.teacher_name
      }
    });
    /* dialogRef.afterClosed().subscribe(result => {
      
    }); */
  }

  async getData() {
    this.showDeadline = true;
    this.gettea = await this.sharedService.getTeacher(
      this.selectFormControl.value
    );
    this.getAll = await this.sharedService.getStudent(
      this.selectFormControl.value
    );
    this.getBook = await this.sharedService.getBooks(
      this.selectFormControl.value
    );
    this.getTeacherData = await this.sharedService.getStudent_Unsign(
      this.selectFormControl.value
    );
    this.getStatusDeadline = this.sharedService
      .getStatusDeadline(this.selectFormControl.value)
      .subscribe(deadLineData => {
        deadLineData.forEach(deadLine => {
          this.isDeadLine = Boolean(deadLine.val);
        });
      });
    this.getCheckBooks();
    this.check_book();
    this.resultStudentUnsign();
    this.nzMessageService.info(
      "คุณเลือกปีการศึกษา" + " " + this.selectFormControl.value
    );
    this.getdate = await this.sharedService.getDate(
      this.selectFormControl.value
    );
    this.getdate.forEach(data => {
      data.forEach(item => {
        this.dateTime = item.endDate;
      });
      this.dateTime = new Date(
        `${this.dateTime.split("T")[0]} ${this.dateTime.split("T")[1]}`
      );
      let deadLine = this.dateTime.toISOString().split(".")[0];

      const countYear = setInterval(event => {
        let dateNow = new Date().toISOString().split(".")[0];
        if (dateNow == deadLine) {
          this.sharedService.addStatus(this.selectFormControl.value);
          clearInterval(countYear);
        } else {
        }
      }, 1000);
    });
  }

  alertMessage() {
    this.check_id = false;
    this.nzMessageService.error("หมดเวลาลงทะเบียน");
  }

  checkteacher() {
    this.gettea.forEach(teacher => {
      this.teaLen = teacher.length;
      this.teacherData = teacher;
      this.secondValidate(this.teaLen);
    });
  }
  resultStudentUnsign() {
    this.getTeacherData.forEach(data => {
      this.getTeacherData = data;
    });
  }
  getCheckBooks() {
    this.getBook.forEach(books => {
      this.book = books;
    });
  }
  checkAllStd() {
    let i = 0;
    if (this.books.id != undefined) {
      this.getAll.forEach(data => {
        for (i; i < data.length; i++) {
          if (this.books.id.trim() == data[i].student_id.trim()) {
            this.validate_change(
              this.books.id.trim(),
              data[i].student_id.trim()
            );
            this.books.name = data[i].name;
            this.books.gpa = data[i].gpa;
            this.check_id = true;
            this.checkStudentUnsign();
            break;
          } else {
            this.validate_change(this.books.id, data[i].student_id);
            this.check_id = false;
          }
        }
      });
    }
  }

  checkStudentUnsign() {
    if (this.getTeacherData.length != 0) {
      this.shows = false;
      this.check_id = false;
      for (let i = 0; i < this.getTeacherData.length; i++) {
        if (this.books.id == this.getTeacherData[i].std_id) {
          this.sharedService.chkUnsign = true;
          this.sharedService.bookTeacherData = this.getTeacherData[
            i
          ].teacher.fullName;
          this.sharedService.bookData = {
            StudentName: this.getTeacherData[i].name,
            Teacher01: "",
            Teacher02: "",
            Teacher03: "",
            Teacher04: "",
            Teacher05: "",
            Teacher06: "",
            Teacher07: "",
            Teacher08: "",
            Teacher09: "",
            Teacher10: "",
            Teacher11: "",
            Teacher12: "",
            Teacher13: "",
            Teacher14: "",
            Teacher15: "",
            Teacher16: "",
            Teacher17: "",
            Teacher18: "",
            Teacher19: "",
            Teacher20: ""
          };
          break;
        } else {
          this.sharedService.chkUnsign = true;
          this.sharedService.bookTeacherData = "";
          this.sharedService.bookData = {
            StudentName: this.books.name,
            Teacher01: "",
            Teacher02: "",
            Teacher03: "",
            Teacher04: "",
            Teacher05: "",
            Teacher06: "",
            Teacher07: "",
            Teacher08: "",
            Teacher09: "",
            Teacher10: "",
            Teacher11: "",
            Teacher12: "",
            Teacher13: "",
            Teacher14: "",
            Teacher15: "",
            Teacher16: "",
            Teacher17: "",
            Teacher18: "",
            Teacher19: "",
            Teacher20: ""
          };
        }
      }
    }
  }
  check_book() {
    this.check_id = false;
    let j = 0;
    let booked = false;
    if (this.book == "" || this.book == undefined) {
      this.checkAllStd();
    } else {
      this.getBook.forEach(dataBook => {
        for (j; j < dataBook.length; j++) {
          if (
            this.books.id !== "" ||
            this.books.id !== null ||
            this.books.id !== undefined
          ) {
            if (this.books.id.trim() == dataBook[j].StudentID) {
              this.sharedService.chkUnsign = false;
              if (dataBook[j].teacher != null) {
                this.teacher_key = dataBook[j].teacher;
                this.sharedService.bookTeacherData = Object.values(
                  this.teacher_key
                )[0];
                this.sharedService.chkBookTeacher = false;
              } else {
                this.sharedService.chkBookTeacher = true;
              }
              this.sharedService.bookData = dataBook[j];
              this.checkteacher();
              booked = false;
              this.shows = false;
              this.notification.error(
                "ลงทะเบียนซ้ำ",
                `คุณได้ลงทะเบียนไปแล้ว! กรุณาตรวจสอบประวัติ`
              );
              break;
            } else {
              booked = true;
              this.shows = true;
            }
          }
        }
        if (booked) {
          if (this.isDeadLine && this.sharedService.chkBookTeacher) {
            this.notification.info(
              "ไม่สามารถลงทะเบียนได้!",
              `ไม่สามารถลงทะเบียนได้ เนื่องจากหมดเวลาการลงทะเบียนแล้ว`
            );
          }
          this.checkAllStd();
        }
      });
    }
  }

  validate_change(input_id, student_id) {
    this.firstFormGroup = this.fg.group({
      firstCtrl: [
        input_id,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(8),
          Validators.pattern(student_id)
        ]
      ]
    });
  }

  addData() {
    this.sharedService.addData(this.books, this.selectFormControl.value);
    this.notification.success(
      "บันทึกสำเร็จ",
      `${this.books.name} กรุณาตรวจสอบประวัติ`
    );
    this.books = new Books();
  }

  secondValidate(teaLen) {
    if (teaLen == null) {
      this.secondFormGroup = this.fg.group({
        secondCtrl1: ["", [Validators.required]],
        secondCtrl2: ["", [Validators.required]],
        secondCtrl3: ["", [Validators.required]],
        secondCtrl4: ["", [Validators.required]],
        secondCtrl5: ["", [Validators.required]],
        secondCtrl6: ["", [Validators.required]],
        secondCtrl7: ["", [Validators.required]],
        secondCtrl8: ["", [Validators.required]],
        secondCtrl9: ["", [Validators.required]],
        secondCtrl10: ["", [Validators.required]]
      });
    } else {
      if (teaLen == 1) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: "",
          secondCtrl3: "",
          secondCtrl4: "",
          secondCtrl5: "",
          secondCtrl6: "",
          secondCtrl7: "",
          secondCtrl8: "",
          secondCtrl9: "",
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: false,
          chkOption03: false,
          chkOption04: false,
          chkOption05: false,
          chkOption06: false,
          chkOption07: false,
          chkOption08: false,
          chkOption09: false,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 2) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: "",
          secondCtrl4: "",
          secondCtrl5: "",
          secondCtrl6: "",
          secondCtrl7: "",
          secondCtrl8: "",
          secondCtrl9: "",
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: false,
          chkOption04: false,
          chkOption05: false,
          chkOption06: false,
          chkOption07: false,
          chkOption08: false,
          chkOption09: false,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 3) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: "",
          secondCtrl5: "",
          secondCtrl6: "",
          secondCtrl7: "",
          secondCtrl8: "",
          secondCtrl9: "",
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: false,
          chkOption05: false,
          chkOption06: false,
          chkOption07: false,
          chkOption08: false,
          chkOption09: false,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 4) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: "",
          secondCtrl6: "",
          secondCtrl7: "",
          secondCtrl8: "",
          secondCtrl9: "",
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: false,
          chkOption06: false,
          chkOption07: false,
          chkOption08: false,
          chkOption09: false,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 5) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: "",
          secondCtrl7: "",
          secondCtrl8: "",
          secondCtrl9: "",
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: false,
          chkOption07: false,
          chkOption08: false,
          chkOption09: false,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 6) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: "",
          secondCtrl8: "",
          secondCtrl9: "",
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: false,
          chkOption08: false,
          chkOption09: false,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 7) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: "",
          secondCtrl9: "",
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: false,
          chkOption09: false,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 8) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: "",
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: false,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 9) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: "",
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: false,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 10) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: "",
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: false,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 11) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: "",
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: false,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 12) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: "",
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: false,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 13) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: [this.books.selectedOption13, [Validators.required]],
          secondCtrl14: "",
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: true,
          chkOption14: false,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 14) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: [this.books.selectedOption13, [Validators.required]],
          secondCtrl14: [this.books.selectedOption14, [Validators.required]],
          secondCtrl15: "",
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: true,
          chkOption14: true,
          chkOption15: false,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 15) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: [this.books.selectedOption13, [Validators.required]],
          secondCtrl14: [this.books.selectedOption14, [Validators.required]],
          secondCtrl15: [this.books.selectedOption15, [Validators.required]],
          secondCtrl16: "",
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: true,
          chkOption14: true,
          chkOption15: true,
          chkOption16: false,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 16) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: [this.books.selectedOption13, [Validators.required]],
          secondCtrl14: [this.books.selectedOption14, [Validators.required]],
          secondCtrl15: [this.books.selectedOption15, [Validators.required]],
          secondCtrl16: [this.books.selectedOption16, [Validators.required]],
          secondCtrl17: "",
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: true,
          chkOption14: true,
          chkOption15: true,
          chkOption16: true,
          chkOption17: false,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 17) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: [this.books.selectedOption13, [Validators.required]],
          secondCtrl14: [this.books.selectedOption14, [Validators.required]],
          secondCtrl15: [this.books.selectedOption15, [Validators.required]],
          secondCtrl16: [this.books.selectedOption16, [Validators.required]],
          secondCtrl17: [this.books.selectedOption17, [Validators.required]],
          secondCtrl18: "",
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: true,
          chkOption14: true,
          chkOption15: true,
          chkOption16: true,
          chkOption17: true,
          chkOption18: false,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 18) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: [this.books.selectedOption13, [Validators.required]],
          secondCtrl14: [this.books.selectedOption14, [Validators.required]],
          secondCtrl15: [this.books.selectedOption15, [Validators.required]],
          secondCtrl16: [this.books.selectedOption16, [Validators.required]],
          secondCtrl17: [this.books.selectedOption17, [Validators.required]],
          secondCtrl18: [this.books.selectedOption18, [Validators.required]],
          secondCtrl19: "",
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: true,
          chkOption14: true,
          chkOption15: true,
          chkOption16: true,
          chkOption17: true,
          chkOption18: true,
          chkOption19: false,
          chkOption20: false
        };
      } else if (teaLen == 19) {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: [this.books.selectedOption13, [Validators.required]],
          secondCtrl14: [this.books.selectedOption14, [Validators.required]],
          secondCtrl15: [this.books.selectedOption15, [Validators.required]],
          secondCtrl16: [this.books.selectedOption16, [Validators.required]],
          secondCtrl17: [this.books.selectedOption17, [Validators.required]],
          secondCtrl18: [this.books.selectedOption18, [Validators.required]],
          secondCtrl19: [this.books.selectedOption19, [Validators.required]],
          secondCtrl20: ""
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: true,
          chkOption14: true,
          chkOption15: true,
          chkOption16: true,
          chkOption17: true,
          chkOption18: true,
          chkOption19: true,
          chkOption20: false
        };
      } else {
        this.secondFormGroup = this.fg.group({
          secondCtrl1: [this.books.selectedOption01, [Validators.required]],
          secondCtrl2: [this.books.selectedOption02, [Validators.required]],
          secondCtrl3: [this.books.selectedOption03, [Validators.required]],
          secondCtrl4: [this.books.selectedOption04, [Validators.required]],
          secondCtrl5: [this.books.selectedOption05, [Validators.required]],
          secondCtrl6: [this.books.selectedOption06, [Validators.required]],
          secondCtrl7: [this.books.selectedOption07, [Validators.required]],
          secondCtrl8: [this.books.selectedOption08, [Validators.required]],
          secondCtrl9: [this.books.selectedOption09, [Validators.required]],
          secondCtrl10: [this.books.selectedOption10, [Validators.required]],
          secondCtrl11: [this.books.selectedOption11, [Validators.required]],
          secondCtrl12: [this.books.selectedOption12, [Validators.required]],
          secondCtrl13: [this.books.selectedOption13, [Validators.required]],
          secondCtrl14: [this.books.selectedOption14, [Validators.required]],
          secondCtrl15: [this.books.selectedOption15, [Validators.required]],
          secondCtrl16: [this.books.selectedOption16, [Validators.required]],
          secondCtrl17: [this.books.selectedOption17, [Validators.required]],
          secondCtrl18: [this.books.selectedOption18, [Validators.required]],
          secondCtrl19: [this.books.selectedOption19, [Validators.required]],
          secondCtrl20: [this.books.selectedOption20, [Validators.required]]
        });
        this.chk_option_val = {
          chkOption01: true,
          chkOption02: true,
          chkOption03: true,
          chkOption04: true,
          chkOption05: true,
          chkOption06: true,
          chkOption07: true,
          chkOption08: true,
          chkOption09: true,
          chkOption10: true,
          chkOption11: true,
          chkOption12: true,
          chkOption13: true,
          chkOption14: true,
          chkOption15: true,
          chkOption16: true,
          chkOption17: true,
          chkOption18: true,
          chkOption19: true,
          chkOption20: true
        };
      }
    }
  }

  callCheckRepeat(teacher, option) {
    if (
      this.books.selectedOption01 == this.books.selectedOption02 &&
      (option == "selectedOption01" || option == "selectedOption02")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption02 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption03 &&
      (option == "selectedOption01" || option == "selectedOption03")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption03 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption04 &&
      (option == "selectedOption01" || option == "selectedOption04")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption04 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption05 &&
      (option == "selectedOption01" || option == "selectedOption05")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption05 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption06 &&
      (option == "selectedOption01" || option == "selectedOption06")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption06 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption07 &&
      (option == "selectedOption01" || option == "selectedOption07")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption07 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption08 &&
      (option == "selectedOption01" || option == "selectedOption08")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption08 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption09 &&
      (option == "selectedOption01" || option == "selectedOption09")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption09 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption10 &&
      (option == "selectedOption01" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption11 &&
      (option == "selectedOption01" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption01 == this.books.selectedOption12 &&
      (option == "selectedOption01" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption01") {
        this.books.selectedOption01 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption03 &&
      (option == "selectedOption02" || option == "selectedOption03")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption03 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption04 &&
      (option == "selectedOption02" || option == "selectedOption04")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption04 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption05 &&
      (option == "selectedOption02" || option == "selectedOption05")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption05 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption06 &&
      (option == "selectedOption02" || option == "selectedOption06")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption06 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption07 &&
      (option == "selectedOption02" || option == "selectedOption07")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption07 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption08 &&
      (option == "selectedOption02" || option == "selectedOption08")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption08 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption09 &&
      (option == "selectedOption02" || option == "selectedOption09")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption09 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption10 &&
      (option == "selectedOption02" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption11 &&
      (option == "selectedOption02" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption02 == this.books.selectedOption12 &&
      (option == "selectedOption02" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption02") {
        this.books.selectedOption02 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption04 &&
      (option == "selectedOption03" || option == "selectedOption04")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption04 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption05 &&
      (option == "selectedOption03" || option == "selectedOption05")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption05 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption06 &&
      (option == "selectedOption03" || option == "selectedOption06")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption06 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption07 &&
      (option == "selectedOption03" || option == "selectedOption07")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption07 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption08 &&
      (option == "selectedOption03" || option == "selectedOption08")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption08 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption09 &&
      (option == "selectedOption03" || option == "selectedOption09")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption09 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption10 &&
      (option == "selectedOption03" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption11 &&
      (option == "selectedOption03" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption03 == this.books.selectedOption12 &&
      (option == "selectedOption03" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption03") {
        this.books.selectedOption03 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption04 == this.books.selectedOption05 &&
      (option == "selectedOption04" || option == "selectedOption05")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption04") {
        this.books.selectedOption04 = null;
      } else {
        this.books.selectedOption05 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption04 == this.books.selectedOption06 &&
      (option == "selectedOption04" || option == "selectedOption06")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption04") {
        this.books.selectedOption04 = null;
      } else {
        this.books.selectedOption06 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption04 == this.books.selectedOption07 &&
      (option == "selectedOption04" || option == "selectedOption07")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption04") {
        this.books.selectedOption04 = null;
      } else {
        this.books.selectedOption07 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption04 == this.books.selectedOption08 &&
      (option == "selectedOption04" || option == "selectedOption08")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption04") {
        this.books.selectedOption04 = null;
      } else {
        this.books.selectedOption08 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption04 == this.books.selectedOption09 &&
      (option == "selectedOption04" || option == "selectedOption09")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption04") {
        this.books.selectedOption04 = null;
      } else {
        this.books.selectedOption09 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption04 == this.books.selectedOption10 &&
      (option == "selectedOption04" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption04") {
        this.books.selectedOption04 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption04 == this.books.selectedOption11 &&
      (option == "selectedOption04" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption04") {
        this.books.selectedOption04 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption04 == this.books.selectedOption12 &&
      (option == "selectedOption04" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption04") {
        this.books.selectedOption04 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption05 == this.books.selectedOption06 &&
      (option == "selectedOption05" || option == "selectedOption06")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption05") {
        this.books.selectedOption05 = null;
      } else {
        this.books.selectedOption06 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption05 == this.books.selectedOption07 &&
      (option == "selectedOption05" || option == "selectedOption07")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption05") {
        this.books.selectedOption05 = null;
      } else {
        this.books.selectedOption07 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption05 == this.books.selectedOption08 &&
      (option == "selectedOption05" || option == "selectedOption08")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption05") {
        this.books.selectedOption05 = null;
      } else {
        this.books.selectedOption08 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption05 == this.books.selectedOption09 &&
      (option == "selectedOption05" || option == "selectedOption09")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption05") {
        this.books.selectedOption05 = null;
      } else {
        this.books.selectedOption09 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption05 == this.books.selectedOption10 &&
      (option == "selectedOption05" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption05") {
        this.books.selectedOption05 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption05 == this.books.selectedOption11 &&
      (option == "selectedOption05" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption05") {
        this.books.selectedOption05 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption05 == this.books.selectedOption12 &&
      (option == "selectedOption05" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption05") {
        this.books.selectedOption05 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption06 == this.books.selectedOption07 &&
      (option == "selectedOption06" || option == "selectedOption07")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption06") {
        this.books.selectedOption06 = null;
      } else {
        this.books.selectedOption07 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption06 == this.books.selectedOption08 &&
      (option == "selectedOption06" || option == "selectedOption08")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption06") {
        this.books.selectedOption06 = null;
      } else {
        this.books.selectedOption08 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption06 == this.books.selectedOption09 &&
      (option == "selectedOption06" || option == "selectedOption09")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption06") {
        this.books.selectedOption06 = null;
      } else {
        this.books.selectedOption09 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption06 == this.books.selectedOption10 &&
      (option == "selectedOption06" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption06") {
        this.books.selectedOption06 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption06 == this.books.selectedOption11 &&
      (option == "selectedOption06" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption06") {
        this.books.selectedOption06 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption06 == this.books.selectedOption12 &&
      (option == "selectedOption06" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption06") {
        this.books.selectedOption06 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption07 == this.books.selectedOption08 &&
      (option == "selectedOption07" || option == "selectedOption08")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption07") {
        this.books.selectedOption07 = null;
      } else {
        this.books.selectedOption08 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption07 == this.books.selectedOption09 &&
      (option == "selectedOption07" || option == "selectedOption09")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption07") {
        this.books.selectedOption07 = null;
      } else {
        this.books.selectedOption09 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption07 == this.books.selectedOption10 &&
      (option == "selectedOption07" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption07") {
        this.books.selectedOption07 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption07 == this.books.selectedOption11 &&
      (option == "selectedOption07" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption07") {
        this.books.selectedOption07 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption07 == this.books.selectedOption12 &&
      (option == "selectedOption07" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption07") {
        this.books.selectedOption07 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption08 == this.books.selectedOption09 &&
      (option == "selectedOption08" || option == "selectedOption09")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption08") {
        this.books.selectedOption08 = null;
      } else {
        this.books.selectedOption09 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption08 == this.books.selectedOption10 &&
      (option == "selectedOption08" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption08") {
        this.books.selectedOption08 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption08 == this.books.selectedOption11 &&
      (option == "selectedOption08" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption08") {
        this.books.selectedOption08 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption08 == this.books.selectedOption12 &&
      (option == "selectedOption08" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption08") {
        this.books.selectedOption08 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption09 == this.books.selectedOption10 &&
      (option == "selectedOption09" || option == "selectedOption10")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption09") {
        this.books.selectedOption09 = null;
      } else {
        this.books.selectedOption10 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption09 == this.books.selectedOption11 &&
      (option == "selectedOption09" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption09") {
        this.books.selectedOption09 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption09 == this.books.selectedOption12 &&
      (option == "selectedOption09" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption09") {
        this.books.selectedOption09 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption10 == this.books.selectedOption11 &&
      (option == "selectedOption10" || option == "selectedOption11")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption10") {
        this.books.selectedOption10 = null;
      } else {
        this.books.selectedOption11 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption10 == this.books.selectedOption12 &&
      (option == "selectedOption10" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption10") {
        this.books.selectedOption10 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    } else if (
      this.books.selectedOption11 == this.books.selectedOption12 &&
      (option == "selectedOption11" || option == "selectedOption12")
    ) {
      this.alertBar(teacher);
      if (option == "selectedOption11") {
        this.books.selectedOption11 = null;
      } else {
        this.books.selectedOption12 = null;
      }
      this.secondValidate(this.teaLen);
    }
  }
  alertBar(teacher) {
    if (teacher != null) {
      this.notification.warning("เลือกอาจารย์ซ้ำ", `${teacher} ถูกเลือกไปแล้ว`);
    }
  }
}

@Component({
  selector: "register-student-dialog",
  templateUrl: "./register-student.dialog.html"
})
export class RegisterStudentDialog {
  chkUnsign = this.sharedService.chkUnsign;
  bookData: any;
  bookTeacherData: any;
  chkBookTeacher = this.sharedService.chkBookTeacher;
  constructor(
    private sharedService: SharedService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  ngOnInit() {
    this.bookData = this.sharedService.bookData;
    this.bookTeacherData = this.sharedService.bookTeacherData;
  }
}
