import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from 'src/app/admin/admin.component';
import { HomepageComponent } from 'src/app/homepage/homepage.component';

import { RegisterStudentComponent } from 'src/app/Student/register-student/register-student.component';
import { DashboardComponent } from 'src/app/admin/dashboard/dashboard.component';
import { TeacherComponent } from 'src/app/teacher/teacher.component';

import { from } from 'rxjs';

const routes: Routes = [
  { path: '', component: HomepageComponent},
  { path: 'Register', component: RegisterStudentComponent },
  { path: 'teacher/:key', component: TeacherComponent },
  { path: 'login', component: AdminComponent },
  { path: 'dashboard/:key', component: DashboardComponent }
];
@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class RoutingModule { }
