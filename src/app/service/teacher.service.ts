import { Injectable, setTestabilityGetter } from '@angular/core';
import { FormGroup, FormControl, Validator, Validators, FormBuilder } from '@angular/forms';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { TeacherComponent, TeacherDialogform } from '../teacher/teacher.component';
import { map } from 'rxjs/operators';
import * as _ from 'lodash'



@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(
    private firebase: AngularFireDatabase,
    public snackBar: MatSnackBar
  ) { }

  dateObj: AngularFireObject<any>;
  teacher: AngularFireObject<any>;
  seatNumber : AngularFireObject<any>;
  dateList: AngularFireList<any>;

  teacherDataList: AngularFireList<any>;
  teacherRegList: AngularFireList<any>;
  seatList: AngularFireList<any>;

  dateReg: date_reg = new date_reg();
  reg: teacher_reg = new teacher_reg();
  data: teacher_data = new teacher_data();

  teacherDataForm: FormGroup = new FormGroup({
    teacher_id: new FormControl('', Validators.required),
    NameTitle: new FormControl('', Validators.required),
    firstName: new FormControl('',Validators.required),
    lastName: new FormControl('',Validators.required)
  });

  firstFormGroup: FormGroup = new FormGroup({
    teacher_id: new FormControl('', Validators.required)
  });
  secondFormGroup: FormGroup = new FormGroup({
    seat: new FormControl('', Validators.required)
  });

  timeFormGroup: FormGroup = new FormGroup({
    $key: new FormControl(null),
    startDate: new FormControl('', Validators.required),
    endDate: new FormControl('', Validators.required)
  });

  yearsForm:FormControl
  

  initializTeacherDataForm(){
    this.teacherDataForm.setValue({
      teacher_id:'' ,
      NameTitle: '',
      firstName: '',
      lastName: ''
    })
  }

  initializTimeFormGroup() {
    this.timeFormGroup.setValue({
      $key: null,
      startDate: '',
      endDate: ''
    })
  }

  initializFormGroup() {
    this.firstFormGroup.setValue({
      teacher_id: ''
    })
    this.secondFormGroup.setValue({
      seat: ''
    })
  }


  initializSeatFormGroup() {
    this.secondFormGroup.setValue({
      seat: ''
    })
  }

  getTeacherData() {
    this.teacherDataList = this.firebase.list('teacher_data');
    return this.teacherDataList.snapshotChanges();
  }

  getTeacherData2() {
    return this.firebase.list('teacher_data')
      .snapshotChanges()
      .pipe(map(changes => {
        return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }));
      }));
  }

//   getTeacherData3(select:string) {
//     return this.firebase.list('academic_years/'+select+'/teacher_data')
//         .snapshotChanges()
//         .pipe(map(changes => {
//             return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }));
//         }));

// }


  getDateReg() {
    this.dateList = this.firebase.list('date_reg');
    return this.dateList.snapshotChanges();
  }
  getDateReg2(select:string) {
    return this.firebase.list('academic_years/'+select+'/date_reg')
      .snapshotChanges()
      .pipe(map(changes => {
        return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }));
      }));
  }

  getTeacherReg() {
    this.teacherRegList = this.firebase.list('teacher_reg');
    return this.teacherRegList.snapshotChanges();
  }

  getTeacherReg2() {
    return this.firebase.list('teacher_reg')
      .snapshotChanges()
      .pipe(map(changes => {
        return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }));
      }));
  }
  getTeacherReg3(select:string) {
    return this.firebase.list('academic_years/'+select+'/teacher_reg')
        .snapshotChanges()
        .pipe(map(changes => {
            return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }));
        }));

}

  getTeacherObj() {
    this.teacher = this.firebase.object('teacher_reg');
    return this.teacher;
  }

  getSeatNumberObj(){
    this.seatNumber = this.firebase.object('seatNumber');
    return this.seatNumber.snapshotChanges();
  }

  getSeatNumber(select:string,id:string){
    this.seatList = this.firebase.list('academic_years/'+select+'/teacher_reg/'+id+'/seatNumber');
    return this.seatList.snapshotChanges();
  }
  
  // getSeatNumber(id: string) {
  //   this.seatList = this.firebase.list(`teacher_reg/` + id + '/seatNumber');
  //   return this.seatList.snapshotChanges();
  // }

insertTeacherData(teacherData:teacher_data){
  var teacherDataRef = this.firebase.list('teacher_data');
  teacherDataRef.set(`${teacherData.teacher_id}`,{
    teacher_id: teacherData.teacher_id,
    NameTitle:teacherData.NameTitle,
    firstName:teacherData.firstName,
    lastName:teacherData.lastName
  })
}

insertDateReg(dateReg: date_reg,select:string) {
  var dateRef = this.firebase.list('academic_years/'+select+'/date_reg')
  dateRef.push({
    startDate: dateReg.startDate,
    endDate: dateReg.endDate
  })
}
  // insertDateReg(dateReg: date_reg) {
  //   this.dateList.push({
  //     startDate: dateReg.startDate,
  //     endDate: dateReg.endDate
  //   })
  // }

  insertTeacherReg(form1: teacher_reg, form2: teacher_reg, teacherReg: teacher_reg,select:string) {
    var teacherRegRef = this.firebase.list('academic_years/'+select+'/teacher_reg');
    teacherRegRef.set(`${teacherReg.teacher_id}`, {
      seat: form2.seat,
      count: teacherReg.count,
      fullName: teacherReg.fullName,
      // dateReg: teacherReg.dateReg
    })
  }

  // insertSeatNumber(seat_number:seat_number,id:string,idStd:string){
  //   var seatRef = this.firebase.list(`teacher_reg/`+id+'/seatNumber');
  //   seatRef.set(`${idStd}`,{
  //     studentId: seat_number.studentId,
  //     // nameStd:seat_number.nameStd,
  //     timeReg:seat_number.timeReg        
  //   })  
  // }

  setTeacherData(teacher_data:teacher_data){
    this.teacherDataForm.setValue(teacher_data);
  }

  setSeatFormGroup(teacher_reg: teacher_reg) {
    this.secondFormGroup.setValue(teacher_reg);
  }


  setTimeFormGroup(date_reg: date_reg) {
    this.timeFormGroup.setValue(date_reg);
  }

  // updateTime(date_reg: date_reg) {
  //   this.dateList.update(date_reg.$key, {
  //     startDate: date_reg.startDate,
  //     endDate: date_reg.endDate
  //   });
  // }
  updateTime(date_reg: date_reg,select:string) {
    var dateRef = this.firebase.list('academic_years/'+select+'/date_reg');
    dateRef.update(date_reg.$key,{
      startDate: date_reg.startDate,
      endDate: date_reg.endDate
    })
  }
  

  // updateTeacherData(teacher_data:teacher_data){
  //   this.teacherDataList.update(`${teacher_data.teacher_id}`,{
  //     teacher_id: teacher_data.teacher_id,
  //     NameTitle:teacher_data.NameTitle,
  //     firstName:teacher_data.firstName,
  //     lastName:teacher_data.lastName
  //   })
  // }

  // updateTeacher(teacher_reg: teacher_reg){
  //   this.teacherList.update(teacher_reg.$key,{
  //     seat:teacher_reg.seat
  //   });

  // }


  deleteTeacherData(id:string) {
    this.teacher = this.firebase.object('teacher_data/'+id)
    this.teacher.remove();
  }

  deleteTeacherReg(select:string,id: string) {
    this.teacher = this.firebase.object('academic_years/'+select+'/teacher_reg/' + id)
    this.teacher.remove();
  }



  // updateTeacher(teacher){
  //   this.teacherList.update(teacher.$key,{
  //     title: teacher.title,
  //     firstName: teacher.firstName,
  //     lastName: teacher.lastName,
  //     seat: teacher.seat,
  //     startRgt:teacher.startRgt,
  //     endRgt:teacher.endRgt
  //   });
  // }

  // deleteTeacher($key: string){
  //   this.teacherList.remove($key);
  // }


  /////snackBar/////
  configSnackBar: MatSnackBarConfig = {
    duration: 3000,
    horizontalPosition: 'right',
    verticalPosition: 'top'
  }
  success(msg) {
    this.snackBar.open(msg, '', this.configSnackBar);
  }
  warning(msg) {
    this.snackBar.open(msg, '', this.configSnackBar);
  }

}

export class teacher_data {
  teacher_id: string;
  NameTitle: string;
  firstName: string;
  lastName: string;
}
export class teacher_reg {
  teacher_id: string;
  fullName: string;
  seat: number;
  count: number;
  dateReg: string;
  seatNumber: seatNumber;
}

export class seatNumber {
  std_id: string;
  nameStd:string;
  // timeReg: string;
}
// export class seat_number{
//   studentId: string = '';
//   nameStd:string = '';
//   timeReg: string = '';
// }

export class date_reg {
  $key: string;
  startDate: Date;
  endDate: Date;
}