import { Injectable } from '@angular/core';
import { AngularFireDatabase,AngularFireList } from 'angularfire2/database';
import { AngularFireAction } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/switchMap';
import { map } from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})

export class AdminService {
    items$: Observable<AngularFireAction<firebase.database.DataSnapshot>[]>;
    size$: BehaviorSubject<string | null>;
    yearsDataList: AngularFireList<any>;
    constructor(private db: AngularFireDatabase) { }
    getAdminData() {
        return this.db.list('admin_data')
            .snapshotChanges()
            .pipe(map(changes => {
                return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }), console.log());
            }));

    }

    getTchLogin() {
        return this.db.list('teacher_login')
            .snapshotChanges()
            .pipe(map(changes => {
                return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }), console.log());
            }));

    }

    getStudentData(select:string) {
        return this.db.list('academic_years/'+select+'/student_data')
            .snapshotChanges()
            .pipe(map(changes => {
                return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }), console.log());
            }));

    }
    getTeacherData() {
        return this.db.list('teacher_data')
            .snapshotChanges()
            .pipe(map(changes => {
                return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }));
            }));
    }

    getStudent_Books(select:string) {
        return this.db.list('academic_years/'+select+'/books')
            .snapshotChanges()
            .pipe(map(changes => {
                return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }), console.log());
            }));
    }
    getTeacher_Reg(select:string) {
        return this.db.list('academic_years/'+select+'/teacher_reg')
            .snapshotChanges()
            .pipe(map(changes => {
                return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }), console.log());
            }));
    }

    getAcdemic_years() {
        return this.db.list('academic_years')
            .snapshotChanges()
            .pipe(map(changes => {
                return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }), console.log());
            }));
    }

    getSelectYear(select:string) {
        return this.db.list('academic_years/'+select)
            .snapshotChanges()
            .pipe(map(changes => {
                return changes.map(item => ({ key: item.payload.key, ...item.payload.val() }), console.log());
            }));
    }

    getAcademicYears() {
        this.yearsDataList = this.db.list('academic_years');
        return this.yearsDataList.snapshotChanges();
      }



}
export class Admin {
    user: string;
    password: string;
}
export class addYear {
    year: string;
}

export class YearsData {
    $key: string;
    status :string;
}