import { Injectable } from '@angular/core';
import {AngularFireDatabase,AngularFireList} from 'angularfire2/database'
import { isNgTemplate } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  titleList : AngularFireList<any>;
  array = [];

  constructor(private firebase: AngularFireDatabase) {
    this.titleList=this.firebase.list('title');
    this.titleList.snapshotChanges().subscribe(
      list => {
        this.array = list.map(item =>{
          return{
            $key: item.key,
            ...item.payload.val()
          };
        });
      });
   }

}
